<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\components;

use yii\base\Component;
use Yii;

/**
 * Description of SecondThread.
 *
 * @author programmer_5
 */
class SecondThread extends Component
{
    private $dynamicallyCreatedFilesDir;

    private $executeFileName;

    private $rootDir;

    private $fileContent = '';

    public function init()
    {
        $this->rootDir = Yii::getAlias('@app');
        $this->dynamicallyCreatedFilesDir = 'projectFiles/secondThreadFiles';
    }

    public function setExecuteFileName($fileName)
    {
        $this->executeFileName = (string) $fileName;
    }

    public function setRootDir($dir)
    {
        $this->rootDir = (string) $dir;
    }

    public function setDynamicallyCreatedFilesDir($dir)
    {
        $this->dynamicallyCreatedFilesDir = (string) $dir;
    }

    public function setFileContent($content)
    {
        $this->fileContent = (string) $content;
    }

    public function setAddFileContent($content)
    {
        $this->fileContent .= (string) $content;
    }

    public function getExecuteFileName()
    {
        return $this->executeFileName;
    }

    public function getRootDir()
    {
        return $this->rootDir;
    }

    public function getDynamicallyCreatedFilesDir()
    {
        return $this->dynamicallyCreatedFilesDir;
    }

    public function getFileContent()
    {
        return $this->fileContent;
    }

    public function getFileContentPHP()
    {
        return '<?php '.PHP_EOL.$this->getFileContent().PHP_EOL.'unlink("'.$this->getFilePat().'");';
    }

    public function getFilePat()
    {
        return $this->getRootDir().'/'.$this->getExecuteFileName();
    }

    public function executeFile()
    {
        $this->exec($this->getFilePat());
    }

    public function exec($fileName)
    {
        //  dump($fileName, 1);
        if (is_file($fileName)) {
            $r = exec('php '.$fileName.' > /dev/null &');
        }
    }

    public function generateExecuteFile()
    {
        $this->setExecuteFileName($this->getDynamicallyCreatedFilesDir().'/'.$this->generateExecuteFileName());
        $pat = file_put_contents($this->getFilePat(), $this->getFileContentPHP());
        // dump(file_get_contents($this->getFilePat()));
        // die('q');
    }

    public function generateExecuteFileName()
    {
        return Yii::$app->security->generateRandomString().'.php';
    }

    public function deleteFile()
    {
        if (is_file($this->getFilePat())) {
            unlink($this->getFilePat());
        }
    }
}
