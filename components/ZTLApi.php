<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\components;

use Yii;
use yii\base\Component;

/**
 * Description of ZTLApi
 *
 * @author programmer_5
 */
class ZTLApi extends Component {
    
    private $model;
    
    private $jsonData;
    
    private $token;
    
    private $status;
    
    private $msg='';
    
    private $data;

    private $id;
    
    private $id_name='id';

    private $action;
    
    private $allowActions;
    
    private $success=true;
    
    private $modelName;


    private $model_aliases=[
        'user' => '\app\models\User',
        'farms' => '\app\models\Farms',
        'drivers' => '\app\models\Drivers',
        'roads' => '\app\models\Roads',
        'contracts' => '\app\models\Contracts',
        'carriers' => '\app\models\Carriers',
    ];
  
    private $msg_array=[
        'server_error' => '',
    ];


    public function init() {
        $json_str = file_get_contents('php://input');
        $this -> jsonData =json_decode($json_str,true);
        $this->loadJson();
    }
    
    public function loadJson(){
         if (isset($this -> jsonData['token']))
            $this -> token  = $this->jsonData['token'];
         if (isset($this -> jsonData[$this->id_name]))
            $this->id = $this -> jsonData[$this->id_name];
        // dump($this -> token,1);
    }

    public function setStatus($key){
        $arr = ['error', 'success'];
        $this->status = isset($arr[$key]) ? $arr[$key] : $arr[0];
        return $this;
    }


    public function setLogs($data=[]){
        if (!$data)
            $data = $this -> jsonData;
        $f = Yii::getAlias('@webroot').'/api_logs.json';
        $json = is_file($f) ? json_decode(file_get_contents($f),true) : [];
        $json[time()] = [
            'dateReq' => date('Y-m-d H:i:s'),
            'data' => $data,
            'model' => $this->modelName,
            'action' => $this->action,
             ];
        krsort($json);
        $json = array_values($json);
       // $json = array_slice($json,0,20); 
        file_put_contents($f, json_encode($json)); 
        return $this;
    }

    public function setModel($name){
        $this->modelName = $name;
        if (is_object($name))
            $this -> model = $name;
        else {
            $name = isset($this->model_aliases[mb_strtolower($name)]) ? $this->model_aliases[mb_strtolower($name)] : $name;
            if (class_exists($name))
               $this -> model = new $name();
        }
        return $this;
    }
    
    public function setJsonData($json){
        $data = is_string($json) ? json_decode($json,true) : $json;
        if (is_array($data) && count($data))
            foreach ($data as $k => $v)
                $this->jsonData[$k] = $v;
        return $this;
    }
    
    public function setAction($action=null){
        $arr=[
            'create'=>'createAPI', 
            'update'=>'updateAPI', 
            'read'=>'readAPI', 
            'list'=>'listAPI', 
            'delete'=>'deleteAPI',
            
            'createAPI'=>'createAPI', 
            'updateAPI'=>'updateAPI', 
            'readAPI'=>'readAPI', 
            'listAPI'=>'listAPI', 
            'deleteAPI'=>'deleteAPI',
            
            ];
       
        if (method_exists($this->model, $action))
            $this->action = $action;
        else if (isset($arr[$action]))
            $this->action = $arr[$action];
        return $this;
    }
    
    public function setAllowActions($actions){
        $this->allowActions = (array)$actions;
        return $this;
    }




    public function setId_name($name){
        $this->id_name = $name;
        return $this;
    }


    public function accessСheck($v=null){
        $this->loadJson();
        
        $userInfo = \app\models\Users::find()->where(['token'=> $this->token])->one();
      //  dump($userInfo,1);
        if (!$userInfo)
            $this->success = false;
        else if (is_array ($v) && count($v)){
            foreach ($v as $key => $val){
                if ($userInfo->$key !== $val){
                    $this->success = false;
                    break;
                }
            }
        }
       // dump($this->success,1);
        return $this;
    }
    
    public function accessСheckDriver($v=null){
        $this->loadJson();
     //   dump($this->token,1);
        $driverInfo = \app\models\Drivers::find()->where(['token'=> $this->token])->one();
        if (!$driverInfo)
            $this->success = false;

        return $this;
    }
    
    public function load(){
       $this->loadJson();
        if (!$this->success){
            $this->msg = $this->getMsgText('permission_denied');
            $this->setStatus(0);
        } else if (!$this -> model){
            $this->msg = $this->getMsgText('class_not_exist');
            $this->setStatus(2);
        } else if (!$this->action || ($this->allowActions && !in_array($this->action, $this->allowActions))){
            $this->msg = $this->getMsgText('method_not_exist');
            $this->setStatus(1);
        } else {
            $action = $this->action;
            $act = method_exists($this->model, $action) ? $this->model -> $action($this) : $this->$action();
            $this->msg = $act['msg'];
            $this->setStatus($act['status']);
            $this->data = $act['data'];
        }
        return $this;
    }
    
    public function createAPI(){
        return $this -> saveData($this->model);
    }
    
    public function readAPI(){
        $data = $this -> model ->find()->where([$this -> id_name=>(string)$this->id])->one();
        if ($data)
            return ['status' => 1,'msg' => 'Success.','data' => $data->toArray(method_exists($data, 'fieldsAPI') ? $data -> fieldsAPI() : [])];
        else
            return ['status' => 0,'msg' => 'Data with such id was not found!','data'=>null];
    }
    
    public function listAPI(){
    //    dump($this->model->relationsList(),1);
       $q = $this -> model ->find()->with(method_exists($this->model, 'relationsList') ? $this->model->relationsList() : []);
       $data = \yii\helpers\ArrayHelper::toArray($q->all());
       return ['status' => 1,'msg' => 'Success.','data' => $data];
    }
    
    public function updateAPI(){
       $obj = $this -> model ->find()->where([$this -> id_name=>(string)$this->id])->one();
       return $this -> saveData($obj);
    }
    
    public function deleteAPI(){
        $res = ['status' => 1,'msg' => 'Successful deletion','data' => null];
        if (is_array($this->id) && $this->id){
            foreach ($this->id as $v)
                if ($obj = $this -> model ->findOne([$this -> id_name=>(int)$v])) $obj->delete();
                
        } else if (!is_array($this->id) && $obj = $this -> model ->findOne((int)$this->id))
                $obj->delete();
        else
            $res = ['status' => 0,'msg' => 'Data with such id was not found!','data' => null];
        Yii::$app->firebase->sendCallUpdateData($this -> model->tableName());
        return $res;
    }


    public function getMsgText($key){
        if (isset($this->msg_array[$key]))
            return $this->msg_array[$key];
        else 
            return $key;
    }
    
    public function convertMsg($key=null){
        $arr=[
            'enter_password' => [
                'User successfully saved' => 'Password successfully entered'
            ]
        ];
        if (isset($arr[$key][$this->msg]))
            $this->msg = $arr[$key][$this->msg];
        return $this;
    }


    public function getMsg(){
        return $this->getMsgText($this->msg);
    }
    
    public function getStatus(){
        return $this->status;
    }
    
    public function getData(){
        return $this->data;
    }
    
    public function getJsonData(){
        return $this->jsonData;
    }
    
    public function getId(){
        return (string)$this->id;
    }
    
    public function getId_name(){
        return $this->id_name;
    }


    public function saveData($obj = null){
        if (!$obj)
           return ['status' => 1,'msg' => 'Data with such id was not found!','data' => null];
        $obj->attributes = $this->jsonData;
        $obj->save();
        return [ 
                'status' => $obj->getErrors() ? 0 : 1,
                'msg' => $obj->getErrors() ? $obj->getErrorsMsg()  : 'Success',
                'data' => $obj->getErrors() ? null : $obj->convertObjToArray(method_exists($obj, 'fieldsAPI') ? $obj -> fieldsAPI() : null)
            ];
        
    }
    
    
}
