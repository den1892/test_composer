<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\components;

use yii\base\Component;
use Yii;

/**
 * Description of Firebase
 *
 * @author programmer_5
 */
class Firebase extends Component {

    //put your code here

    private $key='AAAAhZLmSd8:APA91bHB9canvMjw8HIuKVg6pLwOBEQZV73ZgMp88HbW8xln-pTZGTqoahVUF5-5TFBfItMy7b0l2VWU4-auFu09ZL2Tu3XtdBQNkH8J3et-hCPA5ublPrWdA4z8NpKoAZMothYpqEgU';
    
    public function setKey($key){
        $this->key = $key;
        return $this;
    }

    public function curl($data=null) {

        $headers = ['Authorization: key=' . $this->key, 'Content-Type: application/json'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        if ($data)
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function sendCall($data=null) {
       return $this->curl($data);
    }
    
    public function sendCallUpdateData($field=null){
        $data = [
            '$ch = curl_init();',
            'curl_setopt($ch, CURLOPT_URL, "http://us-central1-zlatatradelogist-1500971806241.cloudfunctions.net/setData?field='.$field.'&key=37b9Jx01SFPEJzTdypt4pEZS4eJHsmxE");',
            'curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);',
            '$res = curl_exec($ch);',
            'curl_close($ch);',
        ];
        
        $objSend = new SecondThread();
        $objSend->setFileContent(implode(PHP_EOL,$data));
        $objSend->generateExecuteFile();
        $objSend->executeFile(); 
  
    }

    
    public function getThreadContent(){
                $data = [
            'include "'.Yii::getAlias('@app').'/components/Firebase.php";',
            '$req = (new \app\components\Firebase()) -> sendCallUpdateDataTheard("'.(string)$field.'");',
            'file_put_contents("'.Yii::getAlias('@app').'/fireBaseLogs.txt",(string)$req);'
        ]; 
    }
    

}
