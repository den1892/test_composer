<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\components;

use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Description of ParseJson.
 *
 * @author programmer_5
 */
class ParseJson extends Component
{
    private $dir;
    public $rules = [
            'NEW_Contract' => [
                'model' => '\app\models\Contracts',
                'fields' => [
                    'UID' => 'uid',
                    'contract_name' => 'name',
                    'firm_uid' => 'firmUID',
                    'culture_uid' => 'cultureUID',
                    'region_uid' => 'receiverRegionUID',
                    'receiver_point_uid' => 'receiverPointUID',
                ],
                'uniqueField' => 'uid',
            ],
            'Contract' => [
                'model' => '\app\models\Contracts',
                'fields' => [
                    'UID' => 'uid',
                    'contract_name' => 'name',
                    'firm_uid' => 'firmUID',
                    'culture_uid' => 'cultureUID',
                ],
                'uniqueField' => 'uid',
            ],
            'firm_uid' => [
                'model' => '\app\models\Firms',
                'fields' => [
                    'uid' => 'uid',
                    'name' => 'name',
                ],
                'uniqueField' => 'uid',
            ],
            'culture_uid' => [
                'model' => '\app\models\Culture',
                'fields' => [
                    'uid' => 'uid',
                    'name' => 'name',
                ],
                'uniqueField' => 'uid',
            ],
            'auto_uid' => [
                'model' => '\app\models\Auto',
                'fields' => [
                    'uid' => 'uid',
                    'name' => 'name',
                    'carrier_uid' => 'carrierUID',
                    'driver_uid' => 'driverUID',
                    'model_uid' => 'modelUID',
                ],
                'uniqueField' => 'uid',
                'add' => [
                    'auto_uid_ADD',
                ],
            ],
            'receiver_point_uid' => [
                'model' => '\app\models\RegionPoints',
                'fields' => [
                    'uid' => 'uid',
                    'name' => 'name',
                    'region_uid' => 'regionUID',
                ],
                'uniqueField' => 'uid',
            ],
            'driver_uid' => [
                'model' => '\app\models\Drivers',
                'fields' => [
                    'uid' => 'uid',
                    'name' => 'name',
                ],
                'uniqueField' => 'uid',
            ],

           'auto_uid_ADD' => [
               'model' => '\app\models\Drivers',
               'fields' => [
                   'driver_uid' => 'uid',
                   'phone' => 'phone',
               ],
               'uniqueField' => 'uid',
           ],

        'region_uid' => [
                'model' => '\app\models\Regions',
                'fields' => [
                    'uid' => 'uid',
                    'name' => 'name',
                ],
                'uniqueField' => 'uid',
            ],
            'carrier_uid' => [
                'model' => '\app\models\Carriers',
                'fields' => [
                    'uid' => 'uid',
                    'name' => 'name',
                ],
                'uniqueField' => 'uid',
            ],
            'model_uid' => [
                'model' => '\app\models\AutoModels',
                'fields' => [
                    'uid' => 'uid',
                    'name' => 'name',
                ],
                'uniqueField' => 'uid',
            ],
            'roads' => [
                'model' => '\app\models\Roads',
                'fields' => [],
                'toArray' => [
                    'id', 'unloadingWeight', 'loadingWeight', 'loadingDate', 'unloadingDate', 'statusID', 'typeID', 'price',
                    'driverUID', 'senderRegionUID', 'senderPointUID', 'receiverRegionUID', 'receiverPointUID', 'contractUID',
                    'ttnNumber', 'autoUID', 'carrierUID', 'createdData',
                ],
            ],
        ];

    // public $rules = array(
    //     'NEW_Contract' => array(
    //         0 => array(
    //             'model' => '\\app\\models\\Contracts',
    //             'fields' => array(
    //                 'UID' => 'uid',
    //                 'contract_name' => 'name',
    //                 'firm_uid' => 'firmUID',
    //                 'culture_uid' => 'cultureUID',
    //                 'region_uid' => 'receiverRegionUID',
    //                 'receiver_point_uid' => 'receiverPointUID',
    //             ),
    //             'uniqueField' => 'uid',
    //         ),
    //     ),
    //     'Contract' => array(
    //         0 => array(
    //             'model' => '\\app\\models\\Contracts',
    //             'fields' => array(
    //                 'UID' => 'uid',
    //                 'contract_name' => 'name',
    //                 'firm_uid' => 'firmUID',
    //                 'culture_uid' => 'cultureUID',
    //             ),
    //             'uniqueField' => 'uid',
    //         ),
    //     ),
    //     'firm_uid' => array(
    //         0 => array(
    //             'model' => '\\app\\models\\Firms',
    //             'fields' => array(
    //                 'uid' => 'uid',
    //                 'name' => 'name',
    //             ),
    //             'uniqueField' => 'uid',
    //         ),
    //     ),
    //     'culture_uid' => array(
    //         0 => array(
    //             'model' => '\\app\\models\\Culture',
    //             'fields' => array(
    //                 'uid' => 'uid',
    //                 'name' => 'name',
    //             ),
    //             'uniqueField' => 'uid',
    //         ),
    //     ),
    //     'auto_uid' => array(
    //         0 => array(
    //             'model' => '\\app\\models\\Auto',
    //             'fields' => array(
    //                 'uid' => 'uid',
    //                 'name' => 'name',
    //                 'carrier_uid' => 'carrierUID',
    //                 'driver_uid' => 'driverUID',
    //                 'model_uid' => 'modelUID',
    //             ),
    //             'uniqueField' => 'uid',
    //         ),
    //         1 => array(
    //             'model' => '\app\models\Drivers',
    //             'fields' => [
    //                 'driver_uid' => 'uid',
    //                 'phone' => 'phone',
    //             ],
    //             'uniqueField' => 'uid',
    //         ),
    //     ),
    //     'receiver_point_uid' => array(
    //         0 => array(
    //             'model' => '\\app\\models\\RegionPoints',
    //             'fields' => array(
    //                 'uid' => 'uid',
    //                 'name' => 'name',
    //                 'region_uid' => 'regionUID',
    //             ),
    //             'uniqueField' => 'uid',
    //         ),
    //     ),
    //     'driver_uid' => array(
    //         0 => array(
    //             'model' => '\\app\\models\\Drivers',
    //             'fields' => array(
    //                 'uid' => 'uid',
    //                 'name' => 'name',
    //             ),
    //             'uniqueField' => 'uid',
    //         ),
    //     ),
    //     'region_uid' => array(
    //         0 => array(
    //             'model' => '\\app\\models\\Regions',
    //             'fields' => array(
    //                 'uid' => 'uid',
    //                 'name' => 'name',
    //             ),
    //             'uniqueField' => 'uid',
    //         ),
    //     ),
    //     'carrier_uid' => array(
    //         0 => array(
    //             'model' => '\\app\\models\\Carriers',
    //             'fields' => array(
    //                 'uid' => 'uid',
    //                 'name' => 'name',
    //             ),
    //             'uniqueField' => 'uid',
    //         ),
    //     ),
    //     'model_uid' => array(
    //         0 => array(
    //             'model' => '\\app\\models\\AutoModels',
    //             'fields' => array(
    //                 'uid' => 'uid',
    //                 'name' => 'name',
    //             ),
    //             'uniqueField' => 'uid',
    //         ),
    //     ),
    //     'roads' => array(
    //         0 => array(
    //             'model' => '\\app\\models\\Roads',
    //             'fields' => array(
    //             ),
    //             'toArray' => array(
    //                 0 => 'id',
    //                 1 => 'unloadingWeight',
    //                 2 => 'loadingWeight',
    //                 3 => 'loadingDate',
    //                 4 => 'unloadingDate',
    //                 5 => 'statusID',
    //                 6 => 'typeID',
    //                 7 => 'price',
    //                 8 => 'driverUID',
    //                 9 => 'senderRegionUID',
    //                 10 => 'senderPointUID',
    //                 11 => 'receiverRegionUID',
    //                 12 => 'receiverPointUID',
    //                 13 => 'contractUID',
    //                 14 => 'ttnNumber',
    //                 15 => 'autoUID',
    //                 16 => 'carrierUID',
    //                 17 => 'createdData',
    //             ),
    //         ),
    //     ),
    // );

    private $onlyRules;

    public function init()
    {
        $this->dir = Yii::getAlias('@app').'/projectFiles/1cFiles/upload_toLogic';
    }

    public function setRules($rules)
    {
        $this->rules = $rules;

        return $this;
    }

    public function setOnlyRules($rules)
    {
        $this->onlyRules = $rules;

        return $this;
    }

    public function setDir($dir)
    {
        $this->dir = Yii::getAlias('@app').'/projectFiles/1cFiles/'.$dir;
    }

    public function getRules($rules = [])
    {
        if (!$this->onlyRules) {
            return $this->rules;
        }
        foreach ($this->onlyRules as $v) {
            if (isset($this->rules[$v])) {
                $rules[$v] = $this->rules[$v];
            }
        }

        return $rules;
    }

    public function getRuleParam($key)
    {
        return ArrayHelper::getValue($this->getRules(), $key);
    }

    public function getModelObj($key)
    {
        $model = ArrayHelper::getValue($this->getRules(), $key.'.model');

        return $model ? new $model() : null;
    }

    public function getTableName($key)
    {
        $model = ArrayHelper::getValue($this->getRules(), $key.'.model');

        return $model ? $model::className() : null;
    }

    public function getToArray($key)
    {
        return ArrayHelper::getValue($this->getRules(), $key.'.toArray', []);
        // return $model ? $model::className() : null;
    }

    public function getUidName($key)
    {
        return ArrayHelper::getValue($this->getRules(), $key.'.uniqueField');
    }

    public function getFields($key)
    {
        return ArrayHelper::getValue($this->getRules(), $key.'.fields');
    }

    public function getField($key, $field)
    {
        return ArrayHelper::getValue($this->getRules(), $key.'.fields.'.$field);
    }

    public function getAllPositions($key)
    {
        $model = $this->getModelObj($key);
        if ($uidName = $this->getUidName($key)) {
            return ArrayHelper::index($model->find()->all(), $uidName);
        }

        return [];
    }

    public function getObj($allPositions, $data, $key)
    {
        $uidName = $this->getUidName($key);
        $uidValue = ArrayHelper::getValue($data, $uidName);

        return ArrayHelper::getValue($allPositions, $uidValue, $this->getModelObj($key));
    }

    public function getJsonInfo()
    {
        $files = scandir($this->dir);
        $res = [];
        foreach ($files as $v) {
            $json = is_file($this->dir.'/'.$v) ? json_decode(file_get_contents($this->dir.'/'.$v), true) : null;

            if ($json) {
                foreach ($json as $k1 => $v1) {
                    $res[$k1] = $v1;
                }
            }
        }

        return $res;
    }

    public function clearDir()
    {
        $files = scandir($this->dir);
        foreach ($files as $v) {
            if (is_file($this->dir.'/'.$v)) {
                file_put_contents(Yii::getAlias('@app').'/projectFiles/1cFiles/backup/'.$v, file_get_contents($this->dir.'/'.$v));

                unlink($this->dir.'/'.$v);
            }
        }
    }

    public function convertJsonData($data, $key)
    {
        $res = [];
        $fields = $this->getFields($key);
        foreach ($data as $k => $v) {
            if ($this->getField($key, $k)) {
                $res[$this->getField($key, $k)] = $v;
            } else {
                $res[$k] = $v;
            }
        }

        return $res;
    }

    public function save()
    {
        $saveObj = [];
        $saveErrors = [];
        $jsonINFO = $this->getJsonInfo();
        //    dump($jsonINFO,1);
        if ($jsonINFO) {
            foreach ($jsonINFO as $k => $v) {
                $this->saveData($k, $v, $$saveObj, $$saveErrors);
            }
        }

        return ['saveObj' => $saveObj, 'saveErrors' => $saveErrors];
    }

    public function saveData($key, $dataSave = null, $saveObj = [], $saveErrors = [])
    {
        if ($this->getModelObj($key) && is_array($dataSave) && $dataSave) {
            $allPositions = $this->getAllPositions($key);

            foreach ($dataSave as $keyData => $data) {
                $data = $this->convertJsonData($data, $key);
                $saveObj[$key][$keyData] = $this->getObj($allPositions, $data, $key);
                $saveObj[$key][$keyData]->attributes = $data;
                $saveObj[$key][$keyData]->save();
                if ($saveObj[$key][$keyData]->getErrors()) {
                    $saveErrors[$key][$keyData] = $saveObj[$key][$keyData]->getErrors();
                }
            }

            if (isset($allPositions['add'])) {
                foreach ($allPositions['add'] as $v) {
                    $saveInfo = $this->saveData($v, $dataSave, $saveObj, $saveErrors);
                    $saveObj = $saveInfo['saveObj'];
                    $saveErrors = $saveInfo['saveErrors'];
                }
            }

            Yii::$app->firebase->sendCallUpdateData($this->getTableName($key));
        }

        return [
           'saveObj' => $saveObj,
           'saveErrors' => $saveErrors,
       ];
    }

    public function queryAll($obj)
    {
        return $obj ? $obj::find()->with(method_exists($obj, 'relationsList') ? $obj->relationsList() : [])->all() : null;
    }

    public function getInfo($res = [])
    {
        $models = $this->getRules();
        if ($models) {
            foreach ($models as $k => $v) {
                $objAll = $this->queryAll($this->getModelObj($k));
                $res[$k] = [];
                if ($objAll) {
                    foreach ($objAll as $fieldInfo) {
                        $res[$k][] = $fieldInfo->toArray($this->getToArray($k));
                    }
                }
            }
        }

        return $res;
    }

    public function saveToJson($info = null, $fileName = null, $fileNameExplicit = null)
    {
        $infoJson = json_encode($info);
        $fileName = $fileNameExplicit ? $fileNameExplicit : date('Y-m-d_H:i:s').'_'.$fileName.rand(1000, 9999).'.json';
        $filePat = $this->dir.'/'.$fileName;

        return file_put_contents($filePat, $infoJson);
    }

    public function saveAllToJson($separateFileForElement = true, $separateFileForPosition = true)
    {
        $info = $this->getInfo();
        if (!$separateFileForElement) {
            return $this->saveToJson($info, 'all_info');
        }
        if ($info) {
            foreach ($info as $k => $v) {
                if (!$separateFileForPosition) {
                    $this->saveToJson($v, $k);
                } elseif ($v && is_array($v)) {
                    foreach ($v as $pos) {
                        $this->saveToJson($pos, $k);
                    }
                }
            }

            return true;
        }

        return false;
    }
}
